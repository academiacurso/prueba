<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 
Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/users', 'UsersController@index')->name('users');
Route::post('/users/store', 'UsersController@store')->name('save_users');
Route::get('/users/grid', 'UsersController@grid')->name('users_grid');
Route::get('/users/delete/{id}', 'UsersController@delete')->name('users_delete');
Route::get('/users/edit/{id}', 'UsersController@edit')->name('users_editar');


Route::get('/reports', 'ReportsController@index')->name('reports');
Route::post('/reports/store', 'ReportsController@store')->name('save_reports');
Route::get('/reports/grid', 'ReportsController@grid')->name('reports_grid');
Route::get('/reports/delete/{id}', 'ReportsController@delete')->name('reports_delete');
Route::get('/reports/edit/{id}', 'ReportsController@edit')->name('reports_editar');

