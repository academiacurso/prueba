<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
Use Response;
Use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Report;
use DataTables;
use Carbon\Carbon;

class ReportsController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function grid(){
        if(Auth::user()->hasAnyRole('admin')){
            $reports=Report::OrderBy('fecha','desc');
    
        }
        else{
            $reports=Report::where('user_id','=',Auth::user()->id)->OrderBy('fecha','desc');
    
        }
       return DataTables::eloquent($reports)
        ->addColumn('usuario', function ($reports) {
           return $reports->users->name;
        })
        ->addColumn('fecha', function ($reports) {
           return   Carbon::parse($reports->fecha)->format('d-m-Y');
        })
        
       
          

        ->addColumn('opciones', function ($reports) {
            $html="";
            $html.='<button type="button" id="formulario_editar" data-valor="'.route('reports_editar',$reports->id).'"  class="btn btn-info btn-sm" title="Editar" onclick="editar('.$reports->id.')"><i class="fa fa-edit"></i></button>';
            $html.='&nbsp;&nbsp;<button id="formulario_eliminar" data-valor="'.route('reports_delete',$reports->id).'" type="button" class="btn btn-warning btn-sm" title="Eliminar" onclick="eliminar('.$reports->id.')"><i class="fa fa-trash"></i></button>';
            return $html;
         })  
        
        ->rawColumns(['usuario','opciones','fecha'])  
        ->make(true);
    }
    public function index(Request $request)
    { 
        $request->user()->authorizeRoles(['admin','empleado']);
    
        return view('reportes/index');
    }    
    
    public function store(Request $request)
    { 
        $request->user()->authorizeRoles(['admin','empleado']); 
        if($request->ajax()){
 

        $rules = array(
                'date'         => 'required',
                'nombre'         => 'required'
            );
        
        $messages = [
            'required' => 'El :attribute campo es requerido.', 
        ];
  
        $validator = Validator::make(Input::all(), $rules, $messages);
  
        if ($validator->fails()) { 
            $error = $validator->errors()->first();
            return Response::json(array('opcion'=>0,'mensaje'=> $error));
        }else{
            if($request->id==0){
                $Report=new Report();
                $Report->fecha=\Carbon\Carbon::parse($request->date)->format('Y-m-d');
                $Report->description=$request->nombre;
                $Report->user_id=Auth::user()->id;
                $Report->save(); 
                return Response::json(array('opcion'=>1,'mensaje'=>'Datos Registrados Correctamente'));
            }
            else{
              
                $Report=Report::find($request->id);    
                $Report->fecha=\Carbon\Carbon::parse($request->date)->format('Y-m-d');
                $Report->description=$request->nombre;
                $Report->save(); 
                return Response::json(array('opcion'=>1,'mensaje'=>'Datos Registrados Correctamente'));
        
            }
        }
    }
    else{
        return Response::json(array('result'=>'No tiene permisos para ver esta pagina'));
    }

    }

    public function delete(Request $request,$id)
    { 
        $request->user()->authorizeRoles(['admin','empleado']); 
        if($request->ajax()){
            $Report=Report::find($id);
            $Report->delete(); 
            return Response::json(array('opcion'=>1,'mensaje'=>'Datos Eliminados Correctamente'));
 
        }
        else{
            return Response::json(array('result'=>'No tiene permisos para ver esta pagina'));
        }

    }

    public function edit(Request $request,$id)
    { 
        $request->user()->authorizeRoles(['admin','empleado']); 
        if($request->ajax()){
            $Report=Report::find($id);
 
            return Response::json(array('opcion'=>1,'mensaje'=>$Report));
 
        }
        else{
            return Response::json(array('result'=>'No tiene permisos para ver esta pagina'));
        }

    }
}

