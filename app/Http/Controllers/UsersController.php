<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Role;
use App\User;
Use Response;
Use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

use DataTables;
use Carbon\Carbon;


class UsersController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function grid(){
        $user=User::where('id','!=',Auth::user()->id)->OrderBy('id','desc');
        return DataTables::eloquent($user)
        ->addColumn('roles', function ($user) {
           return $user->roles()->first()->description;
        })  

        ->addColumn('opciones', function ($user) {
            $html="";
            $html.='<button type="button" id="formulario_editar" data-valor="'.route('users_editar',$user->id).'"  class="btn btn-info btn-sm" title="Editar" onclick="editar('.$user->id.')"><i class="fa fa-edit"></i></button>';
            $html.='&nbsp;&nbsp;<button id="formulario_eliminar" data-valor="'.route('users_delete',$user->id).'" type="button" class="btn btn-warning btn-sm" title="Eliminar" onclick="eliminar('.$user->id.')"><i class="fa fa-trash"></i></button>';
            return $html;
         })  
        
        ->rawColumns(['roles','opciones'])  
        ->make(true);
    }
    public function index(Request $request)
    { 
        $request->user()->authorizeRoles(['admin']);
        $role=Role::select('id','description')->get();
        return view('usuario/index')->withRoles($role);
    }    
    
    public function store(Request $request)
    { 
        $request->user()->authorizeRoles(['admin']); 
        if($request->ajax()){
        if($request->id==0){

            $rules = array(
                'email'         => 'required|email|unique:users',
                'nombre'         => 'required',
                'password'         => 'required',
                'role_id'         => 'required',
            );
        }
        else{
            
            $rules = array(
                'email'         => 'required|email|unique:users,email,'.$request->id,
                'nombre'         => 'required', 
                'role_id'         => 'required',
            );
        }
        $messages = [
            'required' => 'El :attribute campo es requerido.',
            'email' => 'El :attribute debe tener correo valido.',
            'unique' => 'El :attribute esta repetido.',
        ];
  
        $validator = Validator::make(Input::all(), $rules, $messages);
  
        if ($validator->fails()) { 
            $error = $validator->errors()->first();
            return Response::json(array('opcion'=>0,'mensaje'=> $error));
        }else{
            if($request->id==0){
                $user=new User();
                $user->name=$request->nombre;
                $user->email=$request->email;
                $user->password=bcrypt($request->password);
                $user->save();
                DB::table('role_user')->insert([
                    'role_id' => $request->role_id,
                    'user_id' =>  $user->id
                ]);
                return Response::json(array('opcion'=>1,'mensaje'=>'Datos Registrados Correctamente'));
            }
            else{
              
                $user=User::find($request->id);
                $user->name=$request->nombre;
                $user->email=$request->email;
                if(!is_null($request->password)){
                    $user->password=bcrypt($request->password);
                }
                $user->save();
                DB::table('role_user')->where('user_id',$request->id)->delete();
                DB::table('role_user')->insert([
                    'role_id' => $request->role_id,
                    'user_id' =>  $user->id
                ]);  
                return Response::json(array('opcion'=>1,'mensaje'=>'Datos Registrados Correctamente'));
        
            }
        }
    }
    else{
        return Response::json(array('result'=>'No tiene permisos para ver esta pagina'));
    }

    }

    public function delete(Request $request,$id)
    { 
        $request->user()->authorizeRoles(['admin']); 
        if($request->ajax()){
            $user=User::find($id);
            $user->delete();
            DB::table('role_user')->where('user_id',$id)->delete();
            return Response::json(array('opcion'=>1,'mensaje'=>'Datos Eliminados Correctamente'));
 
        }
        else{
            return Response::json(array('result'=>'No tiene permisos para ver esta pagina'));
        }

    }

    public function edit(Request $request,$id)
    { 
        $request->user()->authorizeRoles(['admin']); 
        if($request->ajax()){
            $user=User::find($id);
            $user->rol= DB::table('role_user')->where('user_id',$id)->first()->role_id;
            return Response::json(array('opcion'=>1,'mensaje'=>$user));
 
        }
        else{
            return Response::json(array('result'=>'No tiene permisos para ver esta pagina'));
        }

    }
}
