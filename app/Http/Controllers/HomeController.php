<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Report; 

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
       if(Auth::user()->hasRole('empleado')){
        return view('reportes.index');
       }
       if(Auth::user()->hasRole('admin')){
     
        $users=User::all()->count();
        $report=Report::all()->count();
        $reportlist=Report::whereDate('created_at', '=', date('Y-m-d'))->count();
        return view('home')
                ->with('totalusers', $users)
                ->with('totalreport', $report)
                ->with('totalhoy', $reportlist);
       }
        //$request->user()->authorizeRoles(['admin']);
    }
}
