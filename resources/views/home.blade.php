@extends('layouts.app')

@section('content')
<main class="py-4">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                {{ __('Panel de Dashboard') }}
                </div>

                <div class="card-body">
               <div class="row">
                   <div class="col-md-4">
 
                        <div class="card ">
                            <div class="card-header bg-dark blanco">Total de Usuario</div>
                            <div class="card-body"  style="text-align:center">
                                <h1>{{$totalusers}}</h1>
                            </div>
                            <div class="card-footer">
                             <span class="negro">
                             <i class="fa fa-users"></i> 
                             Listado de Usuarios
                             </span> 

                            </div>
                        </div> 

                   </div>
                   <div class="col-md-4">
                       <div class="card ">
                            <div class="card-header bg-success  blanco">Total de Reportes</div>
                            <div class="card-body"  style="text-align:center">
                                <h1>{{$totalreport}}</h1>
                            </div>
                            <div class="card-footer">
                             <span class="negro">
                             <i class="fa fa-list-alt"></i> 
                             Listado de Reportes
                             </span> 
                            </div>
                        </div> 

                   </div>

                   
                   <div class="col-md-4">
                       <div class="card ">
                            <div class="card-header bg-danger  blanco">Reportes de hoy</div>
                            <div class="card-body" style="text-align:center">
                                <h1>{{$totalhoy}}</h1>
                            </div>
                            <div class="card-footer">
                             <span class="negro">
                             <i class="fa fa-list-alt"></i> 
                             Listado de Reportes de Hoy
                             </span> 
                            </div>
                        </div> 

                   </div>

               </div>   

                </div>
            </div>
        </div>
    </div>
</div>
</main>
@endsection
