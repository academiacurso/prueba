<div class="card">
                <div class="card-header">
                {{ __('Nuevo Usuario') }}
             
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                                <form  id="formulario_users" action="{{route('save_users')}}"  data-toggle="validator" role="form">
                                     @csrf
                                <div class="form-group">
                                    
                                <input type="hidden" name="id" class="form-control" id="iduser" value="0">
                                    <label for="email">Nombres Completo</label>
                                    <input required type="text" name="nombre" class="form-control" id="nombre">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input required type="email" name="email" class="form-control" id="email">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Clave</label>
                                    <input   type="password" name="password" class="form-control" id="pwd">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Tipo de Usuario</label>
                                   <select name="role_id" required class="form-control" id="role_id" >
                                       @foreach($roles as $valores)
                                        <option value="{{$valores->id}}">{{$valores->description}}</option>
                                        @endforeach
                                   </select>
                                </div>
                                <button type="submit" id="button_usuario" class="btn btn-default">Registrar</button>
                              
                                <button type="button" id="button_usuario_cancelar" class="btn btn-default">Cancelar</button>
                                </form>
                        </div>
                    </div>
                </div>
</div>

