@extends('layouts.app')

@section('content')
<main class="py-4">
<div class="container">
    <div class="row justify-content-center">
        
    <div class="col-md-12" id="formulario_usuario">             
                        @include('reportes.partials.formulario')
    </div>

        <div class="col-md-12" id="formulario_table">
            <div class="card">
                <div class="card-header">
                {{ __('Listado de Reportes') }}
             
                </div>

                <div class="card-body">

 
              
                
                    <div class="row">
                    <div class="col-md-9 col-xs-12">

                    </div>
                    <div class="col-md-3 col-xs-12">
                    @if(Auth::user()->hasAnyRole('empleado'))
                        <button class="btn btn-primary form-control"  id="nuevo_usuario">Nuevo Reporte</button>
                    @endif
                    </div>

                    </div><br>
                    <div  class="row">
                    <div class="col-md-12">
                        @include('reportes.partials.table')
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</main>
@endsection 
@section('script')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
<script type="text/javascript">
     
            $("#button_usuario_cancelar").click(function() {
                
              $("#formulario_users")[0].reset();
              $('#formulario_usuario').css('display','none');
              $('#formulario_table').css('display','inline');
            });
            $("#nuevo_usuario").click(function() {
                $("#button_usuario").html('Guardar Datos')
  
              $("#formulario_users")[0].reset();
              $('#formulario_usuario').css('display','inline');
              $('#formulario_table').css('display','none');
            });
            $('#formulario_users').validator().on('submit', function (e) {
                if (e.isDefaultPrevented()) {
                    swal('Mensaje','Error verifique los campos','error');
                } else {
                    e.preventDefault();
                    swal({
                        title: "Esta seguro?",
                        text:"Desea Guardar el Registro",
                        icon: "warning",
                        buttons: {
                            'si': {
                                text: "Si",
                                value: "true",
                            },
                            'no': {
                                text: "No",
                                value: "false",
                            },
                        },
                        closeOnClickOutside:false,
                        closeModal: false 
                    })
                    .then(function (isConfirm) {
                        if(isConfirm=='true'){ 
                            var $form =  $("#formulario_users");
                            e.preventDefault();
                            $.ajax({
                                url: $form.attr('action'),
                                type: 'POST',
                                data: $form.serialize(),
                                success: function(data) {
                                    if(data.opcion==0){
                                      swal('Mensaje',''+data.mensaje+'','error');  
                                    }
                                    if(data.opcion==1){
                                      $("#formulario_users")[0].reset();
                                    $('#formulario_usuario').css('display','none');
                                    $('#formulario_table').css('display','inline');
                                      table.ajax.url("{{route('reports_grid')}}").load();
                                      swal('Mensaje',''+data.mensaje+'','success');  
                                    }
                                      console.log(data);
                                }
                            });
                        }
                    });
                }
            })
 
    
            
  
            var table= $('#tablausuarios').DataTable({
                processing: true,
                serverSide: true,   
                fixedHeader: true,
                responsive: true,
                bAutoWidth: true,
                searching: true,
                "lengthMenu": [
                    [25, 100, 200, -1],
                    [25, 100, 200, "Todos"] // change per page values here
                ],
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.10/i18n/Spanish.json"
                },
    
        ajax: "{{route('reports_grid')}}",
        @if(Auth::user()->hasAnyRole('admin'))
        
        columns: [
            { data: 'fecha', name: 'fecha',searchable:true }, 
            { data: 'description', name: 'description',searchable:true },  
            { data: 'usuario', name: 'usuario',searchable:false }   
        ]  
        @else
        columns: [
            { data: 'fecha', name: 'fecha',searchable:true }, 
            { data: 'description', name: 'description',searchable:true },  
            { data: 'opciones', name: 'opciones',searchable:false }   
        ]  
        @endif
    });
               table
    .order( [ 0, 'desc' ]  )
    .draw();
    
    
  
    function eliminar(id){
        swal({
                        title: "Esta seguro?",
                        text:"Desea Eliminar el Registro",
                        icon: "warning",
                        buttons: {
                            'si': {
                                text: "Si",
                                value: "true",
                            },
                            'no': {
                                text: "No",
                                value: "false",
                            },
                        },
                        closeOnClickOutside:false,
                        closeModal: false 
                    })
                    .then(function (isConfirm) {
                        if(isConfirm=='true'){ 
                            var $form =  $("#formulario_eliminar").attr('data-valor');
                      
                            $.ajax({
                                url: $form,
                                type: 'GET',
                                success: function(data) {
                                    if(data.opcion==0){
                                      swal('Mensaje',''+data.mensaje+'','error');  
                                    }
                                    if(data.opcion==1){
                                    
                                      table.ajax.url("{{route('reports_grid')}}").load();
                                      swal('Mensaje',''+data.mensaje+'','success');  
                                    } 
                                }
                            });
                        }
                    });
    }

    function editar($id) {
        var $form =  $("#formulario_editar").attr('data-valor');
                      
        $.ajax({
                                url: $form,
                                type: 'GET',
                                success: function(data) {
                                    if(data.opcion==0){
                                      swal('Mensaje',''+data.mensaje+'','error');  
                                    }
                                    if(data.opcion==1){
                                      
                                    $('#formulario_usuario').css('display','inline');
                                    $('#formulario_table').css('display','none');
                        
                                    $("#formulario_users")[0].reset();
                                    $("#idreporte").val(data.mensaje.id)
                                    $('#nombre').val(data.mensaje.description); 
                                    $('#date').val(data.mensaje.fecha);  
                                    $("#button_usuario").html('Modificar Datos') 
                                    } 
                                }
                            });
        
    }
</script>

@endsection 