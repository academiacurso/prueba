<table  class="table table-striped" id="tablausuarios">
<thead>
    <tr>
        <td>Fecha</td>
        <td>Descripción</td> 
        @if(Auth::user()->hasAnyRole('admin'))
        <td>Usuario</td>
        @else
        <td>Opciones</td>
        @endif
    </tr>
</thead>
 <tbody>
     
 </tbody>
</table>