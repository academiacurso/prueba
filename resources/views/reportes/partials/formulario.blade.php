<div class="card">
                <div class="card-header">
                {{ __('Nuevo Reporte') }}
             
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                                <form  id="formulario_users" action="{{route('save_reports')}}"  data-toggle="validator" role="form">
                                     @csrf
                                <div class="form-group">
                                    <label for="email">Fecha</label>
                                    <input required type="date" name="date" class="form-control" id="date">
                                </div>
                                
                                <div class="form-group">
                                    
                                <input type="hidden" name="id" class="form-control" id="idreporte" value="0">
                                    <label for="email">Descripción de Reporte</label>
                                    <textarea required type="text" name="nombre" class="form-control" id="nombre"></textarea>
                                    </div>
                               
                                
                                <button type="submit" id="button_usuario" class="btn btn-default">Registrar</button>
                              
                                <button type="button" id="button_usuario_cancelar" class="btn btn-default">Cancelar</button>
                                </form>
                        </div>
                    </div>
                </div>
</div>

