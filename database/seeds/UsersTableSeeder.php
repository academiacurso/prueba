<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $User = new User();
        $User->name ='ADMINISTRADOR DEL SISTEMA';
        $User->email = 'admin@gmail.com';
        $User->password = bcrypt('admin');
        $User->save(); 

        DB::table('role_user')->insert([
            'role_id' => 1,
            'user_id' =>  $User->id
        ]);
    }
}
